/**
 * View Models used by Spring MVC REST controllers.
 */
package setit.delivering.gateway.web.rest.vm;
