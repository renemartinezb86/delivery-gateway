import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { RatingSetitUi } from './rating-setit-ui.model';
import { RatingSetitUiService } from './rating-setit-ui.service';

@Component({
    selector: 'jhi-rating-setit-ui-detail',
    templateUrl: './rating-setit-ui-detail.component.html'
})
export class RatingSetitUiDetailComponent implements OnInit, OnDestroy {

    rating: RatingSetitUi;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private ratingService: RatingSetitUiService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInRatings();
    }

    load(id) {
        this.ratingService.find(id)
            .subscribe((ratingResponse: HttpResponse<RatingSetitUi>) => {
                this.rating = ratingResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInRatings() {
        this.eventSubscriber = this.eventManager.subscribe(
            'ratingListModification',
            (response) => this.load(this.rating.id)
        );
    }
}
