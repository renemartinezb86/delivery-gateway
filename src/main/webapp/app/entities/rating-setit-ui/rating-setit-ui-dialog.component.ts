import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RatingSetitUi } from './rating-setit-ui.model';
import { RatingSetitUiPopupService } from './rating-setit-ui-popup.service';
import { RatingSetitUiService } from './rating-setit-ui.service';
import { ClientOrderSetitUi, ClientOrderSetitUiService } from '../client-order-setit-ui';

@Component({
    selector: 'jhi-rating-setit-ui-dialog',
    templateUrl: './rating-setit-ui-dialog.component.html'
})
export class RatingSetitUiDialogComponent implements OnInit {

    rating: RatingSetitUi;
    isSaving: boolean;

    clientorders: ClientOrderSetitUi[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private ratingService: RatingSetitUiService,
        private clientOrderService: ClientOrderSetitUiService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.clientOrderService
            .query({filter: 'rating-is-null'})
            .subscribe((res: HttpResponse<ClientOrderSetitUi[]>) => {
                if (!this.rating.clientOrderId) {
                    this.clientorders = res.body;
                } else {
                    this.clientOrderService
                        .find(this.rating.clientOrderId)
                        .subscribe((subRes: HttpResponse<ClientOrderSetitUi>) => {
                            this.clientorders = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.rating.id !== undefined) {
            this.subscribeToSaveResponse(
                this.ratingService.update(this.rating));
        } else {
            this.subscribeToSaveResponse(
                this.ratingService.create(this.rating));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<RatingSetitUi>>) {
        result.subscribe((res: HttpResponse<RatingSetitUi>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: RatingSetitUi) {
        this.eventManager.broadcast({ name: 'ratingListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackClientOrderById(index: number, item: ClientOrderSetitUi) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-rating-setit-ui-popup',
    template: ''
})
export class RatingSetitUiPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private ratingPopupService: RatingSetitUiPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.ratingPopupService
                    .open(RatingSetitUiDialogComponent as Component, params['id']);
            } else {
                this.ratingPopupService
                    .open(RatingSetitUiDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
