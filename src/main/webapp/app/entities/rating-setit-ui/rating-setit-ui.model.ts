import { BaseEntity } from './../../shared';

export class RatingSetitUi implements BaseEntity {
    constructor(
        public id?: number,
        public value?: number,
        public comments?: string,
        public clientOrderId?: number,
    ) {
    }
}
