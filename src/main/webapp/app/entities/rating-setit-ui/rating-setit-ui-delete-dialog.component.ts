import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { RatingSetitUi } from './rating-setit-ui.model';
import { RatingSetitUiPopupService } from './rating-setit-ui-popup.service';
import { RatingSetitUiService } from './rating-setit-ui.service';

@Component({
    selector: 'jhi-rating-setit-ui-delete-dialog',
    templateUrl: './rating-setit-ui-delete-dialog.component.html'
})
export class RatingSetitUiDeleteDialogComponent {

    rating: RatingSetitUi;

    constructor(
        private ratingService: RatingSetitUiService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.ratingService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'ratingListModification',
                content: 'Deleted an rating'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-rating-setit-ui-delete-popup',
    template: ''
})
export class RatingSetitUiDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private ratingPopupService: RatingSetitUiPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.ratingPopupService
                .open(RatingSetitUiDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
