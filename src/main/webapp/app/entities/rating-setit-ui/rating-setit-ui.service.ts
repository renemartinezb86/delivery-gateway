import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { RatingSetitUi } from './rating-setit-ui.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<RatingSetitUi>;

@Injectable()
export class RatingSetitUiService {

    private resourceUrl =  SERVER_API_URL + 'microservice/api/ratings';
    private resourceSearchUrl = SERVER_API_URL + 'microservice/api/_search/ratings';

    constructor(private http: HttpClient) { }

    create(rating: RatingSetitUi): Observable<EntityResponseType> {
        const copy = this.convert(rating);
        return this.http.post<RatingSetitUi>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(rating: RatingSetitUi): Observable<EntityResponseType> {
        const copy = this.convert(rating);
        return this.http.put<RatingSetitUi>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<RatingSetitUi>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<RatingSetitUi[]>> {
        const options = createRequestOption(req);
        return this.http.get<RatingSetitUi[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<RatingSetitUi[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<RatingSetitUi[]>> {
        const options = createRequestOption(req);
        return this.http.get<RatingSetitUi[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<RatingSetitUi[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: RatingSetitUi = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<RatingSetitUi[]>): HttpResponse<RatingSetitUi[]> {
        const jsonResponse: RatingSetitUi[] = res.body;
        const body: RatingSetitUi[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to RatingSetitUi.
     */
    private convertItemFromServer(rating: RatingSetitUi): RatingSetitUi {
        const copy: RatingSetitUi = Object.assign({}, rating);
        return copy;
    }

    /**
     * Convert a RatingSetitUi to a JSON which can be sent to the server.
     */
    private convert(rating: RatingSetitUi): RatingSetitUi {
        const copy: RatingSetitUi = Object.assign({}, rating);
        return copy;
    }
}
