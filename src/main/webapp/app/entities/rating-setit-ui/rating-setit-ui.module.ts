import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../shared';
import {
    RatingSetitUiService,
    RatingSetitUiPopupService,
    RatingSetitUiComponent,
    RatingSetitUiDetailComponent,
    RatingSetitUiDialogComponent,
    RatingSetitUiPopupComponent,
    RatingSetitUiDeletePopupComponent,
    RatingSetitUiDeleteDialogComponent,
    ratingRoute,
    ratingPopupRoute,
} from './';

const ENTITY_STATES = [
    ...ratingRoute,
    ...ratingPopupRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        RatingSetitUiComponent,
        RatingSetitUiDetailComponent,
        RatingSetitUiDialogComponent,
        RatingSetitUiDeleteDialogComponent,
        RatingSetitUiPopupComponent,
        RatingSetitUiDeletePopupComponent,
    ],
    entryComponents: [
        RatingSetitUiComponent,
        RatingSetitUiDialogComponent,
        RatingSetitUiPopupComponent,
        RatingSetitUiDeleteDialogComponent,
        RatingSetitUiDeletePopupComponent,
    ],
    providers: [
        RatingSetitUiService,
        RatingSetitUiPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayRatingSetitUiModule {}
