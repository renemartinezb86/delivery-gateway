import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { RatingSetitUiComponent } from './rating-setit-ui.component';
import { RatingSetitUiDetailComponent } from './rating-setit-ui-detail.component';
import { RatingSetitUiPopupComponent } from './rating-setit-ui-dialog.component';
import { RatingSetitUiDeletePopupComponent } from './rating-setit-ui-delete-dialog.component';

export const ratingRoute: Routes = [
    {
        path: 'rating-setit-ui',
        component: RatingSetitUiComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.rating.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'rating-setit-ui/:id',
        component: RatingSetitUiDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.rating.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const ratingPopupRoute: Routes = [
    {
        path: 'rating-setit-ui-new',
        component: RatingSetitUiPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.rating.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'rating-setit-ui/:id/edit',
        component: RatingSetitUiPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.rating.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'rating-setit-ui/:id/delete',
        component: RatingSetitUiDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.rating.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
