import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { LocationSetitUi } from './location-setit-ui.model';
import { LocationSetitUiPopupService } from './location-setit-ui-popup.service';
import { LocationSetitUiService } from './location-setit-ui.service';
import { RegionSetitUi, RegionSetitUiService } from '../region-setit-ui';
import { ProviderSetitUi, ProviderSetitUiService } from '../provider-setit-ui';

@Component({
    selector: 'jhi-location-setit-ui-dialog',
    templateUrl: './location-setit-ui-dialog.component.html'
})
export class LocationSetitUiDialogComponent implements OnInit {

    location: LocationSetitUi;
    isSaving: boolean;

    regions: RegionSetitUi[];

    providers: ProviderSetitUi[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private locationService: LocationSetitUiService,
        private regionService: RegionSetitUiService,
        private providerService: ProviderSetitUiService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.regionService.query()
            .subscribe((res: HttpResponse<RegionSetitUi[]>) => { this.regions = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.providerService.query()
            .subscribe((res: HttpResponse<ProviderSetitUi[]>) => { this.providers = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.location.id !== undefined) {
            this.subscribeToSaveResponse(
                this.locationService.update(this.location));
        } else {
            this.subscribeToSaveResponse(
                this.locationService.create(this.location));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<LocationSetitUi>>) {
        result.subscribe((res: HttpResponse<LocationSetitUi>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: LocationSetitUi) {
        this.eventManager.broadcast({ name: 'locationListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackRegionById(index: number, item: RegionSetitUi) {
        return item.id;
    }

    trackProviderById(index: number, item: ProviderSetitUi) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-location-setit-ui-popup',
    template: ''
})
export class LocationSetitUiPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private locationPopupService: LocationSetitUiPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.locationPopupService
                    .open(LocationSetitUiDialogComponent as Component, params['id']);
            } else {
                this.locationPopupService
                    .open(LocationSetitUiDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
