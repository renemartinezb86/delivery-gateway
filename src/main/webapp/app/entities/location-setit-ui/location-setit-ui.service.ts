import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { LocationSetitUi } from './location-setit-ui.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<LocationSetitUi>;

@Injectable()
export class LocationSetitUiService {

    private resourceUrl =  SERVER_API_URL + 'microservice/api/locations';
    private resourceSearchUrl = SERVER_API_URL + 'microservice/api/_search/locations';

    constructor(private http: HttpClient) { }

    create(location: LocationSetitUi): Observable<EntityResponseType> {
        const copy = this.convert(location);
        return this.http.post<LocationSetitUi>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(location: LocationSetitUi): Observable<EntityResponseType> {
        const copy = this.convert(location);
        return this.http.put<LocationSetitUi>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<LocationSetitUi>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<LocationSetitUi[]>> {
        const options = createRequestOption(req);
        return this.http.get<LocationSetitUi[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<LocationSetitUi[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<LocationSetitUi[]>> {
        const options = createRequestOption(req);
        return this.http.get<LocationSetitUi[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<LocationSetitUi[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: LocationSetitUi = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<LocationSetitUi[]>): HttpResponse<LocationSetitUi[]> {
        const jsonResponse: LocationSetitUi[] = res.body;
        const body: LocationSetitUi[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to LocationSetitUi.
     */
    private convertItemFromServer(location: LocationSetitUi): LocationSetitUi {
        const copy: LocationSetitUi = Object.assign({}, location);
        return copy;
    }

    /**
     * Convert a LocationSetitUi to a JSON which can be sent to the server.
     */
    private convert(location: LocationSetitUi): LocationSetitUi {
        const copy: LocationSetitUi = Object.assign({}, location);
        return copy;
    }
}
