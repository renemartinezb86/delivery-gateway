import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { LocationSetitUi } from './location-setit-ui.model';
import { LocationSetitUiPopupService } from './location-setit-ui-popup.service';
import { LocationSetitUiService } from './location-setit-ui.service';

@Component({
    selector: 'jhi-location-setit-ui-delete-dialog',
    templateUrl: './location-setit-ui-delete-dialog.component.html'
})
export class LocationSetitUiDeleteDialogComponent {

    location: LocationSetitUi;

    constructor(
        private locationService: LocationSetitUiService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.locationService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'locationListModification',
                content: 'Deleted an location'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-location-setit-ui-delete-popup',
    template: ''
})
export class LocationSetitUiDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private locationPopupService: LocationSetitUiPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.locationPopupService
                .open(LocationSetitUiDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
