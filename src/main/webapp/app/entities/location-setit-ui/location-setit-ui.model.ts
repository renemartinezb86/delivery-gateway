import { BaseEntity } from './../../shared';

export class LocationSetitUi implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public postalCode?: string,
        public regionId?: number,
        public providers?: BaseEntity[],
    ) {
    }
}
