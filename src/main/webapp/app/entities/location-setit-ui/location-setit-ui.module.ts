import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../shared';
import {
    LocationSetitUiService,
    LocationSetitUiPopupService,
    LocationSetitUiComponent,
    LocationSetitUiDetailComponent,
    LocationSetitUiDialogComponent,
    LocationSetitUiPopupComponent,
    LocationSetitUiDeletePopupComponent,
    LocationSetitUiDeleteDialogComponent,
    locationRoute,
    locationPopupRoute,
    LocationSetitUiResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...locationRoute,
    ...locationPopupRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        LocationSetitUiComponent,
        LocationSetitUiDetailComponent,
        LocationSetitUiDialogComponent,
        LocationSetitUiDeleteDialogComponent,
        LocationSetitUiPopupComponent,
        LocationSetitUiDeletePopupComponent,
    ],
    entryComponents: [
        LocationSetitUiComponent,
        LocationSetitUiDialogComponent,
        LocationSetitUiPopupComponent,
        LocationSetitUiDeleteDialogComponent,
        LocationSetitUiDeletePopupComponent,
    ],
    providers: [
        LocationSetitUiService,
        LocationSetitUiPopupService,
        LocationSetitUiResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayLocationSetitUiModule {}
