import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ProviderSetitUi } from './provider-setit-ui.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<ProviderSetitUi>;

@Injectable()
export class ProviderSetitUiService {

    private resourceUrl =  SERVER_API_URL + 'microservice/api/providers';
    private resourceSearchUrl = SERVER_API_URL + 'microservice/api/_search/providers';

    constructor(private http: HttpClient) { }

    create(provider: ProviderSetitUi): Observable<EntityResponseType> {
        const copy = this.convert(provider);
        return this.http.post<ProviderSetitUi>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(provider: ProviderSetitUi): Observable<EntityResponseType> {
        const copy = this.convert(provider);
        return this.http.put<ProviderSetitUi>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ProviderSetitUi>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ProviderSetitUi[]>> {
        const options = createRequestOption(req);
        return this.http.get<ProviderSetitUi[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ProviderSetitUi[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<ProviderSetitUi[]>> {
        const options = createRequestOption(req);
        return this.http.get<ProviderSetitUi[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ProviderSetitUi[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ProviderSetitUi = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ProviderSetitUi[]>): HttpResponse<ProviderSetitUi[]> {
        const jsonResponse: ProviderSetitUi[] = res.body;
        const body: ProviderSetitUi[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ProviderSetitUi.
     */
    private convertItemFromServer(provider: ProviderSetitUi): ProviderSetitUi {
        const copy: ProviderSetitUi = Object.assign({}, provider);
        return copy;
    }

    /**
     * Convert a ProviderSetitUi to a JSON which can be sent to the server.
     */
    private convert(provider: ProviderSetitUi): ProviderSetitUi {
        const copy: ProviderSetitUi = Object.assign({}, provider);
        return copy;
    }
}
