import { BaseEntity } from './../../shared';

export const enum ServiceType {
    'WATER',
    'GAS'
}

export class ProviderSetitUi implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public serviceType?: ServiceType,
        public email?: string,
        public phoneNumber?: string,
        public unitPrice?: number,
        public unitFormat?: string,
        public locations?: BaseEntity[],
    ) {
    }
}
