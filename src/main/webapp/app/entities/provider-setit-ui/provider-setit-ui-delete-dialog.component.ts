import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ProviderSetitUi } from './provider-setit-ui.model';
import { ProviderSetitUiPopupService } from './provider-setit-ui-popup.service';
import { ProviderSetitUiService } from './provider-setit-ui.service';

@Component({
    selector: 'jhi-provider-setit-ui-delete-dialog',
    templateUrl: './provider-setit-ui-delete-dialog.component.html'
})
export class ProviderSetitUiDeleteDialogComponent {

    provider: ProviderSetitUi;

    constructor(
        private providerService: ProviderSetitUiService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.providerService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'providerListModification',
                content: 'Deleted an provider'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-provider-setit-ui-delete-popup',
    template: ''
})
export class ProviderSetitUiDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private providerPopupService: ProviderSetitUiPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.providerPopupService
                .open(ProviderSetitUiDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
