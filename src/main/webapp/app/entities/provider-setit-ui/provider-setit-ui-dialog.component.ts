import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ProviderSetitUi } from './provider-setit-ui.model';
import { ProviderSetitUiPopupService } from './provider-setit-ui-popup.service';
import { ProviderSetitUiService } from './provider-setit-ui.service';
import { LocationSetitUi, LocationSetitUiService } from '../location-setit-ui';

@Component({
    selector: 'jhi-provider-setit-ui-dialog',
    templateUrl: './provider-setit-ui-dialog.component.html'
})
export class ProviderSetitUiDialogComponent implements OnInit {

    provider: ProviderSetitUi;
    isSaving: boolean;

    locations: LocationSetitUi[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private providerService: ProviderSetitUiService,
        private locationService: LocationSetitUiService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.locationService.query()
            .subscribe((res: HttpResponse<LocationSetitUi[]>) => { this.locations = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.provider.id !== undefined) {
            this.subscribeToSaveResponse(
                this.providerService.update(this.provider));
        } else {
            this.subscribeToSaveResponse(
                this.providerService.create(this.provider));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ProviderSetitUi>>) {
        result.subscribe((res: HttpResponse<ProviderSetitUi>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ProviderSetitUi) {
        this.eventManager.broadcast({ name: 'providerListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackLocationById(index: number, item: LocationSetitUi) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-provider-setit-ui-popup',
    template: ''
})
export class ProviderSetitUiPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private providerPopupService: ProviderSetitUiPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.providerPopupService
                    .open(ProviderSetitUiDialogComponent as Component, params['id']);
            } else {
                this.providerPopupService
                    .open(ProviderSetitUiDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
