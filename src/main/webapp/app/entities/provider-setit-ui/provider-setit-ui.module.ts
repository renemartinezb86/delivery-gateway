import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../shared';
import {
    ProviderSetitUiService,
    ProviderSetitUiPopupService,
    ProviderSetitUiComponent,
    ProviderSetitUiDetailComponent,
    ProviderSetitUiDialogComponent,
    ProviderSetitUiPopupComponent,
    ProviderSetitUiDeletePopupComponent,
    ProviderSetitUiDeleteDialogComponent,
    providerRoute,
    providerPopupRoute,
} from './';

const ENTITY_STATES = [
    ...providerRoute,
    ...providerPopupRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ProviderSetitUiComponent,
        ProviderSetitUiDetailComponent,
        ProviderSetitUiDialogComponent,
        ProviderSetitUiDeleteDialogComponent,
        ProviderSetitUiPopupComponent,
        ProviderSetitUiDeletePopupComponent,
    ],
    entryComponents: [
        ProviderSetitUiComponent,
        ProviderSetitUiDialogComponent,
        ProviderSetitUiPopupComponent,
        ProviderSetitUiDeleteDialogComponent,
        ProviderSetitUiDeletePopupComponent,
    ],
    providers: [
        ProviderSetitUiService,
        ProviderSetitUiPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayProviderSetitUiModule {}
