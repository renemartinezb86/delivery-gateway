import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { ProviderSetitUiComponent } from './provider-setit-ui.component';
import { ProviderSetitUiDetailComponent } from './provider-setit-ui-detail.component';
import { ProviderSetitUiPopupComponent } from './provider-setit-ui-dialog.component';
import { ProviderSetitUiDeletePopupComponent } from './provider-setit-ui-delete-dialog.component';

export const providerRoute: Routes = [
    {
        path: 'provider-setit-ui',
        component: ProviderSetitUiComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.provider.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'provider-setit-ui/:id',
        component: ProviderSetitUiDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.provider.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const providerPopupRoute: Routes = [
    {
        path: 'provider-setit-ui-new',
        component: ProviderSetitUiPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.provider.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'provider-setit-ui/:id/edit',
        component: ProviderSetitUiPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.provider.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'provider-setit-ui/:id/delete',
        component: ProviderSetitUiDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.provider.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
