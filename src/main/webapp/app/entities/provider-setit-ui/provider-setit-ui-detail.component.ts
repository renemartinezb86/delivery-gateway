import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { ProviderSetitUi } from './provider-setit-ui.model';
import { ProviderSetitUiService } from './provider-setit-ui.service';

@Component({
    selector: 'jhi-provider-setit-ui-detail',
    templateUrl: './provider-setit-ui-detail.component.html'
})
export class ProviderSetitUiDetailComponent implements OnInit, OnDestroy {

    provider: ProviderSetitUi;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private providerService: ProviderSetitUiService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInProviders();
    }

    load(id) {
        this.providerService.find(id)
            .subscribe((providerResponse: HttpResponse<ProviderSetitUi>) => {
                this.provider = providerResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInProviders() {
        this.eventSubscriber = this.eventManager.subscribe(
            'providerListModification',
            (response) => this.load(this.provider.id)
        );
    }
}
