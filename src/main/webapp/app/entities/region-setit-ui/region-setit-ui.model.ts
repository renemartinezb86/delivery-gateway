import { BaseEntity } from './../../shared';

export class RegionSetitUi implements BaseEntity {
    constructor(
        public id?: number,
        public regionName?: string,
        public countryId?: number,
        public locations?: BaseEntity[],
    ) {
    }
}
