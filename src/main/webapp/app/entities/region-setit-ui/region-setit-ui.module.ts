import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../shared';
import {
    RegionSetitUiService,
    RegionSetitUiPopupService,
    RegionSetitUiComponent,
    RegionSetitUiDetailComponent,
    RegionSetitUiDialogComponent,
    RegionSetitUiPopupComponent,
    RegionSetitUiDeletePopupComponent,
    RegionSetitUiDeleteDialogComponent,
    regionRoute,
    regionPopupRoute,
    RegionSetitUiResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...regionRoute,
    ...regionPopupRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        RegionSetitUiComponent,
        RegionSetitUiDetailComponent,
        RegionSetitUiDialogComponent,
        RegionSetitUiDeleteDialogComponent,
        RegionSetitUiPopupComponent,
        RegionSetitUiDeletePopupComponent,
    ],
    entryComponents: [
        RegionSetitUiComponent,
        RegionSetitUiDialogComponent,
        RegionSetitUiPopupComponent,
        RegionSetitUiDeleteDialogComponent,
        RegionSetitUiDeletePopupComponent,
    ],
    providers: [
        RegionSetitUiService,
        RegionSetitUiPopupService,
        RegionSetitUiResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayRegionSetitUiModule {}
