import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RegionSetitUi } from './region-setit-ui.model';
import { RegionSetitUiPopupService } from './region-setit-ui-popup.service';
import { RegionSetitUiService } from './region-setit-ui.service';
import { CountrySetitUi, CountrySetitUiService } from '../country-setit-ui';

@Component({
    selector: 'jhi-region-setit-ui-dialog',
    templateUrl: './region-setit-ui-dialog.component.html'
})
export class RegionSetitUiDialogComponent implements OnInit {

    region: RegionSetitUi;
    isSaving: boolean;

    countries: CountrySetitUi[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private regionService: RegionSetitUiService,
        private countryService: CountrySetitUiService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.countryService.query()
            .subscribe((res: HttpResponse<CountrySetitUi[]>) => { this.countries = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.region.id !== undefined) {
            this.subscribeToSaveResponse(
                this.regionService.update(this.region));
        } else {
            this.subscribeToSaveResponse(
                this.regionService.create(this.region));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<RegionSetitUi>>) {
        result.subscribe((res: HttpResponse<RegionSetitUi>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: RegionSetitUi) {
        this.eventManager.broadcast({ name: 'regionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackCountryById(index: number, item: CountrySetitUi) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-region-setit-ui-popup',
    template: ''
})
export class RegionSetitUiPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private regionPopupService: RegionSetitUiPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.regionPopupService
                    .open(RegionSetitUiDialogComponent as Component, params['id']);
            } else {
                this.regionPopupService
                    .open(RegionSetitUiDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
