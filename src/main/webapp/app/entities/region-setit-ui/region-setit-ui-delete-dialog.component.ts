import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { RegionSetitUi } from './region-setit-ui.model';
import { RegionSetitUiPopupService } from './region-setit-ui-popup.service';
import { RegionSetitUiService } from './region-setit-ui.service';

@Component({
    selector: 'jhi-region-setit-ui-delete-dialog',
    templateUrl: './region-setit-ui-delete-dialog.component.html'
})
export class RegionSetitUiDeleteDialogComponent {

    region: RegionSetitUi;

    constructor(
        private regionService: RegionSetitUiService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.regionService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'regionListModification',
                content: 'Deleted an region'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-region-setit-ui-delete-popup',
    template: ''
})
export class RegionSetitUiDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private regionPopupService: RegionSetitUiPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.regionPopupService
                .open(RegionSetitUiDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
