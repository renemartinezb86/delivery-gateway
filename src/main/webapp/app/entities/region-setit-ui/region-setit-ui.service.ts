import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { RegionSetitUi } from './region-setit-ui.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<RegionSetitUi>;

@Injectable()
export class RegionSetitUiService {

    private resourceUrl =  SERVER_API_URL + 'microservice/api/regions';
    private resourceSearchUrl = SERVER_API_URL + 'microservice/api/_search/regions';

    constructor(private http: HttpClient) { }

    create(region: RegionSetitUi): Observable<EntityResponseType> {
        const copy = this.convert(region);
        return this.http.post<RegionSetitUi>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(region: RegionSetitUi): Observable<EntityResponseType> {
        const copy = this.convert(region);
        return this.http.put<RegionSetitUi>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<RegionSetitUi>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<RegionSetitUi[]>> {
        const options = createRequestOption(req);
        return this.http.get<RegionSetitUi[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<RegionSetitUi[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<RegionSetitUi[]>> {
        const options = createRequestOption(req);
        return this.http.get<RegionSetitUi[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<RegionSetitUi[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: RegionSetitUi = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<RegionSetitUi[]>): HttpResponse<RegionSetitUi[]> {
        const jsonResponse: RegionSetitUi[] = res.body;
        const body: RegionSetitUi[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to RegionSetitUi.
     */
    private convertItemFromServer(region: RegionSetitUi): RegionSetitUi {
        const copy: RegionSetitUi = Object.assign({}, region);
        return copy;
    }

    /**
     * Convert a RegionSetitUi to a JSON which can be sent to the server.
     */
    private convert(region: RegionSetitUi): RegionSetitUi {
        const copy: RegionSetitUi = Object.assign({}, region);
        return copy;
    }
}
