import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ClientSetitUi } from './client-setit-ui.model';
import { ClientSetitUiPopupService } from './client-setit-ui-popup.service';
import { ClientSetitUiService } from './client-setit-ui.service';

@Component({
    selector: 'jhi-client-setit-ui-delete-dialog',
    templateUrl: './client-setit-ui-delete-dialog.component.html'
})
export class ClientSetitUiDeleteDialogComponent {

    client: ClientSetitUi;

    constructor(
        private clientService: ClientSetitUiService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.clientService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'clientListModification',
                content: 'Deleted an client'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-client-setit-ui-delete-popup',
    template: ''
})
export class ClientSetitUiDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientPopupService: ClientSetitUiPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.clientPopupService
                .open(ClientSetitUiDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
