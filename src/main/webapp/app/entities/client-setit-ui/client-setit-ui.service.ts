import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ClientSetitUi } from './client-setit-ui.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<ClientSetitUi>;

@Injectable()
export class ClientSetitUiService {

    private resourceUrl =  SERVER_API_URL + 'microservice/api/clients';
    private resourceSearchUrl = SERVER_API_URL + 'microservice/api/_search/clients';

    constructor(private http: HttpClient) { }

    create(client: ClientSetitUi): Observable<EntityResponseType> {
        const copy = this.convert(client);
        return this.http.post<ClientSetitUi>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(client: ClientSetitUi): Observable<EntityResponseType> {
        const copy = this.convert(client);
        return this.http.put<ClientSetitUi>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ClientSetitUi>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ClientSetitUi[]>> {
        const options = createRequestOption(req);
        return this.http.get<ClientSetitUi[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ClientSetitUi[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<ClientSetitUi[]>> {
        const options = createRequestOption(req);
        return this.http.get<ClientSetitUi[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ClientSetitUi[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ClientSetitUi = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ClientSetitUi[]>): HttpResponse<ClientSetitUi[]> {
        const jsonResponse: ClientSetitUi[] = res.body;
        const body: ClientSetitUi[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ClientSetitUi.
     */
    private convertItemFromServer(client: ClientSetitUi): ClientSetitUi {
        const copy: ClientSetitUi = Object.assign({}, client);
        return copy;
    }

    /**
     * Convert a ClientSetitUi to a JSON which can be sent to the server.
     */
    private convert(client: ClientSetitUi): ClientSetitUi {
        const copy: ClientSetitUi = Object.assign({}, client);
        return copy;
    }
}
