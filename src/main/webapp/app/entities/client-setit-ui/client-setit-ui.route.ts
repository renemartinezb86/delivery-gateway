import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { ClientSetitUiComponent } from './client-setit-ui.component';
import { ClientSetitUiDetailComponent } from './client-setit-ui-detail.component';
import { ClientSetitUiPopupComponent } from './client-setit-ui-dialog.component';
import { ClientSetitUiDeletePopupComponent } from './client-setit-ui-delete-dialog.component';

export const clientRoute: Routes = [
    {
        path: 'client-setit-ui',
        component: ClientSetitUiComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.client.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'client-setit-ui/:id',
        component: ClientSetitUiDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.client.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const clientPopupRoute: Routes = [
    {
        path: 'client-setit-ui-new',
        component: ClientSetitUiPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.client.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-setit-ui/:id/edit',
        component: ClientSetitUiPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.client.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-setit-ui/:id/delete',
        component: ClientSetitUiDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.client.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
