import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../shared';
import {
    ClientSetitUiService,
    ClientSetitUiPopupService,
    ClientSetitUiComponent,
    ClientSetitUiDetailComponent,
    ClientSetitUiDialogComponent,
    ClientSetitUiPopupComponent,
    ClientSetitUiDeletePopupComponent,
    ClientSetitUiDeleteDialogComponent,
    clientRoute,
    clientPopupRoute,
} from './';

const ENTITY_STATES = [
    ...clientRoute,
    ...clientPopupRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ClientSetitUiComponent,
        ClientSetitUiDetailComponent,
        ClientSetitUiDialogComponent,
        ClientSetitUiDeleteDialogComponent,
        ClientSetitUiPopupComponent,
        ClientSetitUiDeletePopupComponent,
    ],
    entryComponents: [
        ClientSetitUiComponent,
        ClientSetitUiDialogComponent,
        ClientSetitUiPopupComponent,
        ClientSetitUiDeleteDialogComponent,
        ClientSetitUiDeletePopupComponent,
    ],
    providers: [
        ClientSetitUiService,
        ClientSetitUiPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayClientSetitUiModule {}
