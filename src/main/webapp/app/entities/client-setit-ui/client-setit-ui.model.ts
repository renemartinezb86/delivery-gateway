import { BaseEntity } from './../../shared';

export const enum PaymentMethod {
    'CASH',
    'DEBIT',
    'CREDIT'
}

export const enum SubmitMethod {
    'SMS',
    'PUSH'
}

export class ClientSetitUi implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public email?: string,
        public phoneNumber?: string,
        public paymentMethod?: PaymentMethod,
        public submitMethod?: SubmitMethod,
        public addresses?: BaseEntity[],
    ) {
    }
}
