import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../shared';
import {
    AddressSetitUiService,
    AddressSetitUiPopupService,
    AddressSetitUiComponent,
    AddressSetitUiDetailComponent,
    AddressSetitUiDialogComponent,
    AddressSetitUiPopupComponent,
    AddressSetitUiDeletePopupComponent,
    AddressSetitUiDeleteDialogComponent,
    addressRoute,
    addressPopupRoute,
} from './';

const ENTITY_STATES = [
    ...addressRoute,
    ...addressPopupRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        AddressSetitUiComponent,
        AddressSetitUiDetailComponent,
        AddressSetitUiDialogComponent,
        AddressSetitUiDeleteDialogComponent,
        AddressSetitUiPopupComponent,
        AddressSetitUiDeletePopupComponent,
    ],
    entryComponents: [
        AddressSetitUiComponent,
        AddressSetitUiDialogComponent,
        AddressSetitUiPopupComponent,
        AddressSetitUiDeleteDialogComponent,
        AddressSetitUiDeletePopupComponent,
    ],
    providers: [
        AddressSetitUiService,
        AddressSetitUiPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayAddressSetitUiModule {}
