import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { AddressSetitUiComponent } from './address-setit-ui.component';
import { AddressSetitUiDetailComponent } from './address-setit-ui-detail.component';
import { AddressSetitUiPopupComponent } from './address-setit-ui-dialog.component';
import { AddressSetitUiDeletePopupComponent } from './address-setit-ui-delete-dialog.component';

export const addressRoute: Routes = [
    {
        path: 'address-setit-ui',
        component: AddressSetitUiComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.address.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'address-setit-ui/:id',
        component: AddressSetitUiDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.address.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const addressPopupRoute: Routes = [
    {
        path: 'address-setit-ui-new',
        component: AddressSetitUiPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.address.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'address-setit-ui/:id/edit',
        component: AddressSetitUiPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.address.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'address-setit-ui/:id/delete',
        component: AddressSetitUiDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.address.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
