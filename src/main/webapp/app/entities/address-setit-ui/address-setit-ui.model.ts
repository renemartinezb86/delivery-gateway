import { BaseEntity } from './../../shared';

export const enum AddressType {
    'CONDOMINIUM',
    'HOUSE',
    'BUILDING'
}

export class AddressSetitUi implements BaseEntity {
    constructor(
        public id?: number,
        public value?: string,
        public type?: AddressType,
        public info?: string,
        public clientId?: number,
    ) {
    }
}
