import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { AddressSetitUi } from './address-setit-ui.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<AddressSetitUi>;

@Injectable()
export class AddressSetitUiService {

    private resourceUrl =  SERVER_API_URL + 'microservice/api/addresses';
    private resourceSearchUrl = SERVER_API_URL + 'microservice/api/_search/addresses';

    constructor(private http: HttpClient) { }

    create(address: AddressSetitUi): Observable<EntityResponseType> {
        const copy = this.convert(address);
        return this.http.post<AddressSetitUi>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(address: AddressSetitUi): Observable<EntityResponseType> {
        const copy = this.convert(address);
        return this.http.put<AddressSetitUi>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<AddressSetitUi>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<AddressSetitUi[]>> {
        const options = createRequestOption(req);
        return this.http.get<AddressSetitUi[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<AddressSetitUi[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<AddressSetitUi[]>> {
        const options = createRequestOption(req);
        return this.http.get<AddressSetitUi[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<AddressSetitUi[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: AddressSetitUi = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<AddressSetitUi[]>): HttpResponse<AddressSetitUi[]> {
        const jsonResponse: AddressSetitUi[] = res.body;
        const body: AddressSetitUi[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to AddressSetitUi.
     */
    private convertItemFromServer(address: AddressSetitUi): AddressSetitUi {
        const copy: AddressSetitUi = Object.assign({}, address);
        return copy;
    }

    /**
     * Convert a AddressSetitUi to a JSON which can be sent to the server.
     */
    private convert(address: AddressSetitUi): AddressSetitUi {
        const copy: AddressSetitUi = Object.assign({}, address);
        return copy;
    }
}
