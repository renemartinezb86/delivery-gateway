import { BaseEntity } from './../../shared';

export const enum ServiceType {
    'WATER',
    'GAS'
}

export const enum OrderStatus {
    'OPENED',
    'RECEIVED',
    'DELIVERING',
    'COMPLETED'
}

export class ClientOrderSetitUi implements BaseEntity {
    constructor(
        public id?: number,
        public title?: string,
        public createDate?: any,
        public requestDate?: any,
        public fulfilmentDate?: any,
        public serviceType?: ServiceType,
        public quantity?: number,
        public totalPrice?: number,
        public status?: OrderStatus,
        public providerId?: number,
        public addressId?: number,
    ) {
    }
}
