import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ClientOrderSetitUi } from './client-order-setit-ui.model';
import { ClientOrderSetitUiService } from './client-order-setit-ui.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-client-order-setit-ui',
    templateUrl: './client-order-setit-ui.component.html'
})
export class ClientOrderSetitUiComponent implements OnInit, OnDestroy {
clientOrders: ClientOrderSetitUi[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        private clientOrderService: ClientOrderSetitUiService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal
    ) {
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.clientOrderService.search({
                query: this.currentSearch,
                }).subscribe(
                    (res: HttpResponse<ClientOrderSetitUi[]>) => this.clientOrders = res.body,
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
       }
        this.clientOrderService.query().subscribe(
            (res: HttpResponse<ClientOrderSetitUi[]>) => {
                this.clientOrders = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInClientOrders();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ClientOrderSetitUi) {
        return item.id;
    }
    registerChangeInClientOrders() {
        this.eventSubscriber = this.eventManager.subscribe('clientOrderListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
