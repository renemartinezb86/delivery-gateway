import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { ClientOrderSetitUiComponent } from './client-order-setit-ui.component';
import { ClientOrderSetitUiDetailComponent } from './client-order-setit-ui-detail.component';
import { ClientOrderSetitUiPopupComponent } from './client-order-setit-ui-dialog.component';
import { ClientOrderSetitUiDeletePopupComponent } from './client-order-setit-ui-delete-dialog.component';

export const clientOrderRoute: Routes = [
    {
        path: 'client-order-setit-ui',
        component: ClientOrderSetitUiComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.clientOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'client-order-setit-ui/:id',
        component: ClientOrderSetitUiDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.clientOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const clientOrderPopupRoute: Routes = [
    {
        path: 'client-order-setit-ui-new',
        component: ClientOrderSetitUiPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.clientOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-order-setit-ui/:id/edit',
        component: ClientOrderSetitUiPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.clientOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-order-setit-ui/:id/delete',
        component: ClientOrderSetitUiDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.clientOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
