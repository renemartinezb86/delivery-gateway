import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../shared';
import {
    ClientOrderSetitUiService,
    ClientOrderSetitUiPopupService,
    ClientOrderSetitUiComponent,
    ClientOrderSetitUiDetailComponent,
    ClientOrderSetitUiDialogComponent,
    ClientOrderSetitUiPopupComponent,
    ClientOrderSetitUiDeletePopupComponent,
    ClientOrderSetitUiDeleteDialogComponent,
    clientOrderRoute,
    clientOrderPopupRoute,
} from './';

const ENTITY_STATES = [
    ...clientOrderRoute,
    ...clientOrderPopupRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ClientOrderSetitUiComponent,
        ClientOrderSetitUiDetailComponent,
        ClientOrderSetitUiDialogComponent,
        ClientOrderSetitUiDeleteDialogComponent,
        ClientOrderSetitUiPopupComponent,
        ClientOrderSetitUiDeletePopupComponent,
    ],
    entryComponents: [
        ClientOrderSetitUiComponent,
        ClientOrderSetitUiDialogComponent,
        ClientOrderSetitUiPopupComponent,
        ClientOrderSetitUiDeleteDialogComponent,
        ClientOrderSetitUiDeletePopupComponent,
    ],
    providers: [
        ClientOrderSetitUiService,
        ClientOrderSetitUiPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayClientOrderSetitUiModule {}
