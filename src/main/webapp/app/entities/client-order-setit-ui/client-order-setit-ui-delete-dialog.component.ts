import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ClientOrderSetitUi } from './client-order-setit-ui.model';
import { ClientOrderSetitUiPopupService } from './client-order-setit-ui-popup.service';
import { ClientOrderSetitUiService } from './client-order-setit-ui.service';

@Component({
    selector: 'jhi-client-order-setit-ui-delete-dialog',
    templateUrl: './client-order-setit-ui-delete-dialog.component.html'
})
export class ClientOrderSetitUiDeleteDialogComponent {

    clientOrder: ClientOrderSetitUi;

    constructor(
        private clientOrderService: ClientOrderSetitUiService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.clientOrderService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'clientOrderListModification',
                content: 'Deleted an clientOrder'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-client-order-setit-ui-delete-popup',
    template: ''
})
export class ClientOrderSetitUiDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientOrderPopupService: ClientOrderSetitUiPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.clientOrderPopupService
                .open(ClientOrderSetitUiDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
