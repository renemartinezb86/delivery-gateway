import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { ClientOrderSetitUi } from './client-order-setit-ui.model';
import { ClientOrderSetitUiService } from './client-order-setit-ui.service';

@Injectable()
export class ClientOrderSetitUiPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private clientOrderService: ClientOrderSetitUiService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.clientOrderService.find(id)
                    .subscribe((clientOrderResponse: HttpResponse<ClientOrderSetitUi>) => {
                        const clientOrder: ClientOrderSetitUi = clientOrderResponse.body;
                        if (clientOrder.createDate) {
                            clientOrder.createDate = {
                                year: clientOrder.createDate.getFullYear(),
                                month: clientOrder.createDate.getMonth() + 1,
                                day: clientOrder.createDate.getDate()
                            };
                        }
                        if (clientOrder.requestDate) {
                            clientOrder.requestDate = {
                                year: clientOrder.requestDate.getFullYear(),
                                month: clientOrder.requestDate.getMonth() + 1,
                                day: clientOrder.requestDate.getDate()
                            };
                        }
                        if (clientOrder.fulfilmentDate) {
                            clientOrder.fulfilmentDate = {
                                year: clientOrder.fulfilmentDate.getFullYear(),
                                month: clientOrder.fulfilmentDate.getMonth() + 1,
                                day: clientOrder.fulfilmentDate.getDate()
                            };
                        }
                        this.ngbModalRef = this.clientOrderModalRef(component, clientOrder);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.clientOrderModalRef(component, new ClientOrderSetitUi());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    clientOrderModalRef(component: Component, clientOrder: ClientOrderSetitUi): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.clientOrder = clientOrder;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
