import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ClientOrderSetitUi } from './client-order-setit-ui.model';
import { ClientOrderSetitUiPopupService } from './client-order-setit-ui-popup.service';
import { ClientOrderSetitUiService } from './client-order-setit-ui.service';
import { ProviderSetitUi, ProviderSetitUiService } from '../provider-setit-ui';
import { AddressSetitUi, AddressSetitUiService } from '../address-setit-ui';

@Component({
    selector: 'jhi-client-order-setit-ui-dialog',
    templateUrl: './client-order-setit-ui-dialog.component.html'
})
export class ClientOrderSetitUiDialogComponent implements OnInit {

    clientOrder: ClientOrderSetitUi;
    isSaving: boolean;

    providers: ProviderSetitUi[];

    addresses: AddressSetitUi[];
    createDateDp: any;
    requestDateDp: any;
    fulfilmentDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private clientOrderService: ClientOrderSetitUiService,
        private providerService: ProviderSetitUiService,
        private addressService: AddressSetitUiService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.providerService
            .query({filter: 'clientorder-is-null'})
            .subscribe((res: HttpResponse<ProviderSetitUi[]>) => {
                if (!this.clientOrder.providerId) {
                    this.providers = res.body;
                } else {
                    this.providerService
                        .find(this.clientOrder.providerId)
                        .subscribe((subRes: HttpResponse<ProviderSetitUi>) => {
                            this.providers = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.addressService
            .query({filter: 'clientorder-is-null'})
            .subscribe((res: HttpResponse<AddressSetitUi[]>) => {
                if (!this.clientOrder.addressId) {
                    this.addresses = res.body;
                } else {
                    this.addressService
                        .find(this.clientOrder.addressId)
                        .subscribe((subRes: HttpResponse<AddressSetitUi>) => {
                            this.addresses = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.clientOrder.id !== undefined) {
            this.subscribeToSaveResponse(
                this.clientOrderService.update(this.clientOrder));
        } else {
            this.subscribeToSaveResponse(
                this.clientOrderService.create(this.clientOrder));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ClientOrderSetitUi>>) {
        result.subscribe((res: HttpResponse<ClientOrderSetitUi>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ClientOrderSetitUi) {
        this.eventManager.broadcast({ name: 'clientOrderListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackProviderById(index: number, item: ProviderSetitUi) {
        return item.id;
    }

    trackAddressById(index: number, item: AddressSetitUi) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-client-order-setit-ui-popup',
    template: ''
})
export class ClientOrderSetitUiPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientOrderPopupService: ClientOrderSetitUiPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.clientOrderPopupService
                    .open(ClientOrderSetitUiDialogComponent as Component, params['id']);
            } else {
                this.clientOrderPopupService
                    .open(ClientOrderSetitUiDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
