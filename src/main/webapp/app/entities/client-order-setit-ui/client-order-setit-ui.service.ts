import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { ClientOrderSetitUi } from './client-order-setit-ui.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<ClientOrderSetitUi>;

@Injectable()
export class ClientOrderSetitUiService {

    private resourceUrl =  SERVER_API_URL + 'microservice/api/client-orders';
    private resourceSearchUrl = SERVER_API_URL + 'microservice/api/_search/client-orders';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(clientOrder: ClientOrderSetitUi): Observable<EntityResponseType> {
        const copy = this.convert(clientOrder);
        return this.http.post<ClientOrderSetitUi>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(clientOrder: ClientOrderSetitUi): Observable<EntityResponseType> {
        const copy = this.convert(clientOrder);
        return this.http.put<ClientOrderSetitUi>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ClientOrderSetitUi>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ClientOrderSetitUi[]>> {
        const options = createRequestOption(req);
        return this.http.get<ClientOrderSetitUi[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ClientOrderSetitUi[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<ClientOrderSetitUi[]>> {
        const options = createRequestOption(req);
        return this.http.get<ClientOrderSetitUi[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ClientOrderSetitUi[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ClientOrderSetitUi = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ClientOrderSetitUi[]>): HttpResponse<ClientOrderSetitUi[]> {
        const jsonResponse: ClientOrderSetitUi[] = res.body;
        const body: ClientOrderSetitUi[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ClientOrderSetitUi.
     */
    private convertItemFromServer(clientOrder: ClientOrderSetitUi): ClientOrderSetitUi {
        const copy: ClientOrderSetitUi = Object.assign({}, clientOrder);
        copy.createDate = this.dateUtils
            .convertLocalDateFromServer(clientOrder.createDate);
        copy.requestDate = this.dateUtils
            .convertLocalDateFromServer(clientOrder.requestDate);
        copy.fulfilmentDate = this.dateUtils
            .convertLocalDateFromServer(clientOrder.fulfilmentDate);
        return copy;
    }

    /**
     * Convert a ClientOrderSetitUi to a JSON which can be sent to the server.
     */
    private convert(clientOrder: ClientOrderSetitUi): ClientOrderSetitUi {
        const copy: ClientOrderSetitUi = Object.assign({}, clientOrder);
        copy.createDate = this.dateUtils
            .convertLocalDateToServer(clientOrder.createDate);
        copy.requestDate = this.dateUtils
            .convertLocalDateToServer(clientOrder.requestDate);
        copy.fulfilmentDate = this.dateUtils
            .convertLocalDateToServer(clientOrder.fulfilmentDate);
        return copy;
    }
}
