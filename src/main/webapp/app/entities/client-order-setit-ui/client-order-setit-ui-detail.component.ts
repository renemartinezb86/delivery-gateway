import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { ClientOrderSetitUi } from './client-order-setit-ui.model';
import { ClientOrderSetitUiService } from './client-order-setit-ui.service';

@Component({
    selector: 'jhi-client-order-setit-ui-detail',
    templateUrl: './client-order-setit-ui-detail.component.html'
})
export class ClientOrderSetitUiDetailComponent implements OnInit, OnDestroy {

    clientOrder: ClientOrderSetitUi;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private clientOrderService: ClientOrderSetitUiService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInClientOrders();
    }

    load(id) {
        this.clientOrderService.find(id)
            .subscribe((clientOrderResponse: HttpResponse<ClientOrderSetitUi>) => {
                this.clientOrder = clientOrderResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInClientOrders() {
        this.eventSubscriber = this.eventManager.subscribe(
            'clientOrderListModification',
            (response) => this.load(this.clientOrder.id)
        );
    }
}
