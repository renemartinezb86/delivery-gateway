import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../shared';
import {
    CountrySetitUiService,
    CountrySetitUiPopupService,
    CountrySetitUiComponent,
    CountrySetitUiDetailComponent,
    CountrySetitUiDialogComponent,
    CountrySetitUiPopupComponent,
    CountrySetitUiDeletePopupComponent,
    CountrySetitUiDeleteDialogComponent,
    countryRoute,
    countryPopupRoute,
    CountrySetitUiResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...countryRoute,
    ...countryPopupRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CountrySetitUiComponent,
        CountrySetitUiDetailComponent,
        CountrySetitUiDialogComponent,
        CountrySetitUiDeleteDialogComponent,
        CountrySetitUiPopupComponent,
        CountrySetitUiDeletePopupComponent,
    ],
    entryComponents: [
        CountrySetitUiComponent,
        CountrySetitUiDialogComponent,
        CountrySetitUiPopupComponent,
        CountrySetitUiDeleteDialogComponent,
        CountrySetitUiDeletePopupComponent,
    ],
    providers: [
        CountrySetitUiService,
        CountrySetitUiPopupService,
        CountrySetitUiResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayCountrySetitUiModule {}
