import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { CountrySetitUi } from './country-setit-ui.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<CountrySetitUi>;

@Injectable()
export class CountrySetitUiService {

    private resourceUrl =  SERVER_API_URL + 'microservice/api/countries';
    private resourceSearchUrl = SERVER_API_URL + 'microservice/api/_search/countries';

    constructor(private http: HttpClient) { }

    create(country: CountrySetitUi): Observable<EntityResponseType> {
        const copy = this.convert(country);
        return this.http.post<CountrySetitUi>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(country: CountrySetitUi): Observable<EntityResponseType> {
        const copy = this.convert(country);
        return this.http.put<CountrySetitUi>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<CountrySetitUi>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<CountrySetitUi[]>> {
        const options = createRequestOption(req);
        return this.http.get<CountrySetitUi[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<CountrySetitUi[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<CountrySetitUi[]>> {
        const options = createRequestOption(req);
        return this.http.get<CountrySetitUi[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<CountrySetitUi[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: CountrySetitUi = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<CountrySetitUi[]>): HttpResponse<CountrySetitUi[]> {
        const jsonResponse: CountrySetitUi[] = res.body;
        const body: CountrySetitUi[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to CountrySetitUi.
     */
    private convertItemFromServer(country: CountrySetitUi): CountrySetitUi {
        const copy: CountrySetitUi = Object.assign({}, country);
        return copy;
    }

    /**
     * Convert a CountrySetitUi to a JSON which can be sent to the server.
     */
    private convert(country: CountrySetitUi): CountrySetitUi {
        const copy: CountrySetitUi = Object.assign({}, country);
        return copy;
    }
}
