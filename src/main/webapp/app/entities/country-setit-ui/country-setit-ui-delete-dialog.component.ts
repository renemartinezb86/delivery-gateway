import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CountrySetitUi } from './country-setit-ui.model';
import { CountrySetitUiPopupService } from './country-setit-ui-popup.service';
import { CountrySetitUiService } from './country-setit-ui.service';

@Component({
    selector: 'jhi-country-setit-ui-delete-dialog',
    templateUrl: './country-setit-ui-delete-dialog.component.html'
})
export class CountrySetitUiDeleteDialogComponent {

    country: CountrySetitUi;

    constructor(
        private countryService: CountrySetitUiService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.countryService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'countryListModification',
                content: 'Deleted an country'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-country-setit-ui-delete-popup',
    template: ''
})
export class CountrySetitUiDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private countryPopupService: CountrySetitUiPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.countryPopupService
                .open(CountrySetitUiDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
