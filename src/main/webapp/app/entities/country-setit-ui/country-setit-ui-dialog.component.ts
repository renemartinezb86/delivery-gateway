import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CountrySetitUi } from './country-setit-ui.model';
import { CountrySetitUiPopupService } from './country-setit-ui-popup.service';
import { CountrySetitUiService } from './country-setit-ui.service';

@Component({
    selector: 'jhi-country-setit-ui-dialog',
    templateUrl: './country-setit-ui-dialog.component.html'
})
export class CountrySetitUiDialogComponent implements OnInit {

    country: CountrySetitUi;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private countryService: CountrySetitUiService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.country.id !== undefined) {
            this.subscribeToSaveResponse(
                this.countryService.update(this.country));
        } else {
            this.subscribeToSaveResponse(
                this.countryService.create(this.country));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<CountrySetitUi>>) {
        result.subscribe((res: HttpResponse<CountrySetitUi>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: CountrySetitUi) {
        this.eventManager.broadcast({ name: 'countryListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-country-setit-ui-popup',
    template: ''
})
export class CountrySetitUiPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private countryPopupService: CountrySetitUiPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.countryPopupService
                    .open(CountrySetitUiDialogComponent as Component, params['id']);
            } else {
                this.countryPopupService
                    .open(CountrySetitUiDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
