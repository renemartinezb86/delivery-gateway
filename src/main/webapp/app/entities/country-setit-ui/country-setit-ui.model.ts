import { BaseEntity } from './../../shared';

export class CountrySetitUi implements BaseEntity {
    constructor(
        public id?: number,
        public countryName?: string,
        public regions?: BaseEntity[],
    ) {
    }
}
