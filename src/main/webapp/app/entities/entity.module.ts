import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { GatewayCountrySetitUiModule } from './country-setit-ui/country-setit-ui.module';
import { GatewayRegionSetitUiModule } from './region-setit-ui/region-setit-ui.module';
import { GatewayLocationSetitUiModule } from './location-setit-ui/location-setit-ui.module';
import { GatewayAddressSetitUiModule } from './address-setit-ui/address-setit-ui.module';
import { GatewayClientOrderSetitUiModule } from './client-order-setit-ui/client-order-setit-ui.module';
import { GatewayRatingSetitUiModule } from './rating-setit-ui/rating-setit-ui.module';
import { GatewayProviderSetitUiModule } from './provider-setit-ui/provider-setit-ui.module';
import { GatewayClientSetitUiModule } from './client-setit-ui/client-setit-ui.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        GatewayCountrySetitUiModule,
        GatewayRegionSetitUiModule,
        GatewayLocationSetitUiModule,
        GatewayAddressSetitUiModule,
        GatewayClientOrderSetitUiModule,
        GatewayRatingSetitUiModule,
        GatewayProviderSetitUiModule,
        GatewayClientSetitUiModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayEntityModule {}
