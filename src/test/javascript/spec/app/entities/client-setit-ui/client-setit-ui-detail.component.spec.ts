/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { GatewayTestModule } from '../../../test.module';
import { ClientSetitUiDetailComponent } from '../../../../../../main/webapp/app/entities/client-setit-ui/client-setit-ui-detail.component';
import { ClientSetitUiService } from '../../../../../../main/webapp/app/entities/client-setit-ui/client-setit-ui.service';
import { ClientSetitUi } from '../../../../../../main/webapp/app/entities/client-setit-ui/client-setit-ui.model';

describe('Component Tests', () => {

    describe('ClientSetitUi Management Detail Component', () => {
        let comp: ClientSetitUiDetailComponent;
        let fixture: ComponentFixture<ClientSetitUiDetailComponent>;
        let service: ClientSetitUiService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [ClientSetitUiDetailComponent],
                providers: [
                    ClientSetitUiService
                ]
            })
            .overrideTemplate(ClientSetitUiDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ClientSetitUiDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClientSetitUiService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new ClientSetitUi(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.client).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
