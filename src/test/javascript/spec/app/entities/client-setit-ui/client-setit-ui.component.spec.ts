/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GatewayTestModule } from '../../../test.module';
import { ClientSetitUiComponent } from '../../../../../../main/webapp/app/entities/client-setit-ui/client-setit-ui.component';
import { ClientSetitUiService } from '../../../../../../main/webapp/app/entities/client-setit-ui/client-setit-ui.service';
import { ClientSetitUi } from '../../../../../../main/webapp/app/entities/client-setit-ui/client-setit-ui.model';

describe('Component Tests', () => {

    describe('ClientSetitUi Management Component', () => {
        let comp: ClientSetitUiComponent;
        let fixture: ComponentFixture<ClientSetitUiComponent>;
        let service: ClientSetitUiService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [ClientSetitUiComponent],
                providers: [
                    ClientSetitUiService
                ]
            })
            .overrideTemplate(ClientSetitUiComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ClientSetitUiComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClientSetitUiService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new ClientSetitUi(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.clients[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
