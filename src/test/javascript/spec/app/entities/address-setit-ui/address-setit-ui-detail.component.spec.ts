/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { GatewayTestModule } from '../../../test.module';
import { AddressSetitUiDetailComponent } from '../../../../../../main/webapp/app/entities/address-setit-ui/address-setit-ui-detail.component';
import { AddressSetitUiService } from '../../../../../../main/webapp/app/entities/address-setit-ui/address-setit-ui.service';
import { AddressSetitUi } from '../../../../../../main/webapp/app/entities/address-setit-ui/address-setit-ui.model';

describe('Component Tests', () => {

    describe('AddressSetitUi Management Detail Component', () => {
        let comp: AddressSetitUiDetailComponent;
        let fixture: ComponentFixture<AddressSetitUiDetailComponent>;
        let service: AddressSetitUiService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [AddressSetitUiDetailComponent],
                providers: [
                    AddressSetitUiService
                ]
            })
            .overrideTemplate(AddressSetitUiDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AddressSetitUiDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AddressSetitUiService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new AddressSetitUi(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.address).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
