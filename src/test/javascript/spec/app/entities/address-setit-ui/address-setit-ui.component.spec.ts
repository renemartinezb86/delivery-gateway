/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GatewayTestModule } from '../../../test.module';
import { AddressSetitUiComponent } from '../../../../../../main/webapp/app/entities/address-setit-ui/address-setit-ui.component';
import { AddressSetitUiService } from '../../../../../../main/webapp/app/entities/address-setit-ui/address-setit-ui.service';
import { AddressSetitUi } from '../../../../../../main/webapp/app/entities/address-setit-ui/address-setit-ui.model';

describe('Component Tests', () => {

    describe('AddressSetitUi Management Component', () => {
        let comp: AddressSetitUiComponent;
        let fixture: ComponentFixture<AddressSetitUiComponent>;
        let service: AddressSetitUiService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [AddressSetitUiComponent],
                providers: [
                    AddressSetitUiService
                ]
            })
            .overrideTemplate(AddressSetitUiComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AddressSetitUiComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AddressSetitUiService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new AddressSetitUi(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.addresses[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
