/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { GatewayTestModule } from '../../../test.module';
import { ProviderSetitUiDetailComponent } from '../../../../../../main/webapp/app/entities/provider-setit-ui/provider-setit-ui-detail.component';
import { ProviderSetitUiService } from '../../../../../../main/webapp/app/entities/provider-setit-ui/provider-setit-ui.service';
import { ProviderSetitUi } from '../../../../../../main/webapp/app/entities/provider-setit-ui/provider-setit-ui.model';

describe('Component Tests', () => {

    describe('ProviderSetitUi Management Detail Component', () => {
        let comp: ProviderSetitUiDetailComponent;
        let fixture: ComponentFixture<ProviderSetitUiDetailComponent>;
        let service: ProviderSetitUiService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [ProviderSetitUiDetailComponent],
                providers: [
                    ProviderSetitUiService
                ]
            })
            .overrideTemplate(ProviderSetitUiDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ProviderSetitUiDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProviderSetitUiService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new ProviderSetitUi(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.provider).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
