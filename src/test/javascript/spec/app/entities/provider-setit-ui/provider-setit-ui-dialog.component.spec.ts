/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { GatewayTestModule } from '../../../test.module';
import { ProviderSetitUiDialogComponent } from '../../../../../../main/webapp/app/entities/provider-setit-ui/provider-setit-ui-dialog.component';
import { ProviderSetitUiService } from '../../../../../../main/webapp/app/entities/provider-setit-ui/provider-setit-ui.service';
import { ProviderSetitUi } from '../../../../../../main/webapp/app/entities/provider-setit-ui/provider-setit-ui.model';
import { LocationSetitUiService } from '../../../../../../main/webapp/app/entities/location-setit-ui';

describe('Component Tests', () => {

    describe('ProviderSetitUi Management Dialog Component', () => {
        let comp: ProviderSetitUiDialogComponent;
        let fixture: ComponentFixture<ProviderSetitUiDialogComponent>;
        let service: ProviderSetitUiService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [ProviderSetitUiDialogComponent],
                providers: [
                    LocationSetitUiService,
                    ProviderSetitUiService
                ]
            })
            .overrideTemplate(ProviderSetitUiDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ProviderSetitUiDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProviderSetitUiService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ProviderSetitUi(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.provider = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'providerListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ProviderSetitUi();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.provider = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'providerListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
