/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GatewayTestModule } from '../../../test.module';
import { ProviderSetitUiComponent } from '../../../../../../main/webapp/app/entities/provider-setit-ui/provider-setit-ui.component';
import { ProviderSetitUiService } from '../../../../../../main/webapp/app/entities/provider-setit-ui/provider-setit-ui.service';
import { ProviderSetitUi } from '../../../../../../main/webapp/app/entities/provider-setit-ui/provider-setit-ui.model';

describe('Component Tests', () => {

    describe('ProviderSetitUi Management Component', () => {
        let comp: ProviderSetitUiComponent;
        let fixture: ComponentFixture<ProviderSetitUiComponent>;
        let service: ProviderSetitUiService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [ProviderSetitUiComponent],
                providers: [
                    ProviderSetitUiService
                ]
            })
            .overrideTemplate(ProviderSetitUiComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ProviderSetitUiComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProviderSetitUiService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new ProviderSetitUi(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.providers[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
