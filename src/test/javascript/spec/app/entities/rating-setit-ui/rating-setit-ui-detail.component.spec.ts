/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { GatewayTestModule } from '../../../test.module';
import { RatingSetitUiDetailComponent } from '../../../../../../main/webapp/app/entities/rating-setit-ui/rating-setit-ui-detail.component';
import { RatingSetitUiService } from '../../../../../../main/webapp/app/entities/rating-setit-ui/rating-setit-ui.service';
import { RatingSetitUi } from '../../../../../../main/webapp/app/entities/rating-setit-ui/rating-setit-ui.model';

describe('Component Tests', () => {

    describe('RatingSetitUi Management Detail Component', () => {
        let comp: RatingSetitUiDetailComponent;
        let fixture: ComponentFixture<RatingSetitUiDetailComponent>;
        let service: RatingSetitUiService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [RatingSetitUiDetailComponent],
                providers: [
                    RatingSetitUiService
                ]
            })
            .overrideTemplate(RatingSetitUiDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RatingSetitUiDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RatingSetitUiService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new RatingSetitUi(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.rating).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
