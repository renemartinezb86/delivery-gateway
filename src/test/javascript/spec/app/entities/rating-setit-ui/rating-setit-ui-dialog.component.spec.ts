/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { GatewayTestModule } from '../../../test.module';
import { RatingSetitUiDialogComponent } from '../../../../../../main/webapp/app/entities/rating-setit-ui/rating-setit-ui-dialog.component';
import { RatingSetitUiService } from '../../../../../../main/webapp/app/entities/rating-setit-ui/rating-setit-ui.service';
import { RatingSetitUi } from '../../../../../../main/webapp/app/entities/rating-setit-ui/rating-setit-ui.model';
import { ClientOrderSetitUiService } from '../../../../../../main/webapp/app/entities/client-order-setit-ui';

describe('Component Tests', () => {

    describe('RatingSetitUi Management Dialog Component', () => {
        let comp: RatingSetitUiDialogComponent;
        let fixture: ComponentFixture<RatingSetitUiDialogComponent>;
        let service: RatingSetitUiService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [RatingSetitUiDialogComponent],
                providers: [
                    ClientOrderSetitUiService,
                    RatingSetitUiService
                ]
            })
            .overrideTemplate(RatingSetitUiDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RatingSetitUiDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RatingSetitUiService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new RatingSetitUi(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.rating = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'ratingListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new RatingSetitUi();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.rating = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'ratingListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
