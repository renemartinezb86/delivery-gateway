/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GatewayTestModule } from '../../../test.module';
import { RatingSetitUiComponent } from '../../../../../../main/webapp/app/entities/rating-setit-ui/rating-setit-ui.component';
import { RatingSetitUiService } from '../../../../../../main/webapp/app/entities/rating-setit-ui/rating-setit-ui.service';
import { RatingSetitUi } from '../../../../../../main/webapp/app/entities/rating-setit-ui/rating-setit-ui.model';

describe('Component Tests', () => {

    describe('RatingSetitUi Management Component', () => {
        let comp: RatingSetitUiComponent;
        let fixture: ComponentFixture<RatingSetitUiComponent>;
        let service: RatingSetitUiService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [RatingSetitUiComponent],
                providers: [
                    RatingSetitUiService
                ]
            })
            .overrideTemplate(RatingSetitUiComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RatingSetitUiComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RatingSetitUiService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new RatingSetitUi(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.ratings[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
