/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { GatewayTestModule } from '../../../test.module';
import { RegionSetitUiDialogComponent } from '../../../../../../main/webapp/app/entities/region-setit-ui/region-setit-ui-dialog.component';
import { RegionSetitUiService } from '../../../../../../main/webapp/app/entities/region-setit-ui/region-setit-ui.service';
import { RegionSetitUi } from '../../../../../../main/webapp/app/entities/region-setit-ui/region-setit-ui.model';
import { CountrySetitUiService } from '../../../../../../main/webapp/app/entities/country-setit-ui';

describe('Component Tests', () => {

    describe('RegionSetitUi Management Dialog Component', () => {
        let comp: RegionSetitUiDialogComponent;
        let fixture: ComponentFixture<RegionSetitUiDialogComponent>;
        let service: RegionSetitUiService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [RegionSetitUiDialogComponent],
                providers: [
                    CountrySetitUiService,
                    RegionSetitUiService
                ]
            })
            .overrideTemplate(RegionSetitUiDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RegionSetitUiDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RegionSetitUiService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new RegionSetitUi(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.region = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'regionListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new RegionSetitUi();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.region = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'regionListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
