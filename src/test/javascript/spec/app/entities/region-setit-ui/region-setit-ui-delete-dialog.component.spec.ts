/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { GatewayTestModule } from '../../../test.module';
import { RegionSetitUiDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/region-setit-ui/region-setit-ui-delete-dialog.component';
import { RegionSetitUiService } from '../../../../../../main/webapp/app/entities/region-setit-ui/region-setit-ui.service';

describe('Component Tests', () => {

    describe('RegionSetitUi Management Delete Component', () => {
        let comp: RegionSetitUiDeleteDialogComponent;
        let fixture: ComponentFixture<RegionSetitUiDeleteDialogComponent>;
        let service: RegionSetitUiService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [RegionSetitUiDeleteDialogComponent],
                providers: [
                    RegionSetitUiService
                ]
            })
            .overrideTemplate(RegionSetitUiDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RegionSetitUiDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RegionSetitUiService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
