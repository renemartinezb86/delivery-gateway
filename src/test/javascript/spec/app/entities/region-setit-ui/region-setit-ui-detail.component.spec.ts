/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { GatewayTestModule } from '../../../test.module';
import { RegionSetitUiDetailComponent } from '../../../../../../main/webapp/app/entities/region-setit-ui/region-setit-ui-detail.component';
import { RegionSetitUiService } from '../../../../../../main/webapp/app/entities/region-setit-ui/region-setit-ui.service';
import { RegionSetitUi } from '../../../../../../main/webapp/app/entities/region-setit-ui/region-setit-ui.model';

describe('Component Tests', () => {

    describe('RegionSetitUi Management Detail Component', () => {
        let comp: RegionSetitUiDetailComponent;
        let fixture: ComponentFixture<RegionSetitUiDetailComponent>;
        let service: RegionSetitUiService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [RegionSetitUiDetailComponent],
                providers: [
                    RegionSetitUiService
                ]
            })
            .overrideTemplate(RegionSetitUiDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RegionSetitUiDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RegionSetitUiService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new RegionSetitUi(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.region).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
