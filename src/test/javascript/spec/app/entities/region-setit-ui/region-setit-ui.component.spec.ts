/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GatewayTestModule } from '../../../test.module';
import { RegionSetitUiComponent } from '../../../../../../main/webapp/app/entities/region-setit-ui/region-setit-ui.component';
import { RegionSetitUiService } from '../../../../../../main/webapp/app/entities/region-setit-ui/region-setit-ui.service';
import { RegionSetitUi } from '../../../../../../main/webapp/app/entities/region-setit-ui/region-setit-ui.model';

describe('Component Tests', () => {

    describe('RegionSetitUi Management Component', () => {
        let comp: RegionSetitUiComponent;
        let fixture: ComponentFixture<RegionSetitUiComponent>;
        let service: RegionSetitUiService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [RegionSetitUiComponent],
                providers: [
                    RegionSetitUiService
                ]
            })
            .overrideTemplate(RegionSetitUiComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RegionSetitUiComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RegionSetitUiService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new RegionSetitUi(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.regions[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
