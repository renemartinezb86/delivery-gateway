/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GatewayTestModule } from '../../../test.module';
import { CountrySetitUiComponent } from '../../../../../../main/webapp/app/entities/country-setit-ui/country-setit-ui.component';
import { CountrySetitUiService } from '../../../../../../main/webapp/app/entities/country-setit-ui/country-setit-ui.service';
import { CountrySetitUi } from '../../../../../../main/webapp/app/entities/country-setit-ui/country-setit-ui.model';

describe('Component Tests', () => {

    describe('CountrySetitUi Management Component', () => {
        let comp: CountrySetitUiComponent;
        let fixture: ComponentFixture<CountrySetitUiComponent>;
        let service: CountrySetitUiService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [CountrySetitUiComponent],
                providers: [
                    CountrySetitUiService
                ]
            })
            .overrideTemplate(CountrySetitUiComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CountrySetitUiComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CountrySetitUiService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new CountrySetitUi(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.countries[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
