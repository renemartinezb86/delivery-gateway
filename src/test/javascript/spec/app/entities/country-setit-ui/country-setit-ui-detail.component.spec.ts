/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { GatewayTestModule } from '../../../test.module';
import { CountrySetitUiDetailComponent } from '../../../../../../main/webapp/app/entities/country-setit-ui/country-setit-ui-detail.component';
import { CountrySetitUiService } from '../../../../../../main/webapp/app/entities/country-setit-ui/country-setit-ui.service';
import { CountrySetitUi } from '../../../../../../main/webapp/app/entities/country-setit-ui/country-setit-ui.model';

describe('Component Tests', () => {

    describe('CountrySetitUi Management Detail Component', () => {
        let comp: CountrySetitUiDetailComponent;
        let fixture: ComponentFixture<CountrySetitUiDetailComponent>;
        let service: CountrySetitUiService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [CountrySetitUiDetailComponent],
                providers: [
                    CountrySetitUiService
                ]
            })
            .overrideTemplate(CountrySetitUiDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CountrySetitUiDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CountrySetitUiService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new CountrySetitUi(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.country).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
