/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { GatewayTestModule } from '../../../test.module';
import { ClientOrderSetitUiDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/client-order-setit-ui/client-order-setit-ui-delete-dialog.component';
import { ClientOrderSetitUiService } from '../../../../../../main/webapp/app/entities/client-order-setit-ui/client-order-setit-ui.service';

describe('Component Tests', () => {

    describe('ClientOrderSetitUi Management Delete Component', () => {
        let comp: ClientOrderSetitUiDeleteDialogComponent;
        let fixture: ComponentFixture<ClientOrderSetitUiDeleteDialogComponent>;
        let service: ClientOrderSetitUiService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [ClientOrderSetitUiDeleteDialogComponent],
                providers: [
                    ClientOrderSetitUiService
                ]
            })
            .overrideTemplate(ClientOrderSetitUiDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ClientOrderSetitUiDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClientOrderSetitUiService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
