/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GatewayTestModule } from '../../../test.module';
import { ClientOrderSetitUiComponent } from '../../../../../../main/webapp/app/entities/client-order-setit-ui/client-order-setit-ui.component';
import { ClientOrderSetitUiService } from '../../../../../../main/webapp/app/entities/client-order-setit-ui/client-order-setit-ui.service';
import { ClientOrderSetitUi } from '../../../../../../main/webapp/app/entities/client-order-setit-ui/client-order-setit-ui.model';

describe('Component Tests', () => {

    describe('ClientOrderSetitUi Management Component', () => {
        let comp: ClientOrderSetitUiComponent;
        let fixture: ComponentFixture<ClientOrderSetitUiComponent>;
        let service: ClientOrderSetitUiService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [ClientOrderSetitUiComponent],
                providers: [
                    ClientOrderSetitUiService
                ]
            })
            .overrideTemplate(ClientOrderSetitUiComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ClientOrderSetitUiComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClientOrderSetitUiService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new ClientOrderSetitUi(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.clientOrders[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
