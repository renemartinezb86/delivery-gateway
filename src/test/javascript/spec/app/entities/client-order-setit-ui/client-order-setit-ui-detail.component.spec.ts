/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { GatewayTestModule } from '../../../test.module';
import { ClientOrderSetitUiDetailComponent } from '../../../../../../main/webapp/app/entities/client-order-setit-ui/client-order-setit-ui-detail.component';
import { ClientOrderSetitUiService } from '../../../../../../main/webapp/app/entities/client-order-setit-ui/client-order-setit-ui.service';
import { ClientOrderSetitUi } from '../../../../../../main/webapp/app/entities/client-order-setit-ui/client-order-setit-ui.model';

describe('Component Tests', () => {

    describe('ClientOrderSetitUi Management Detail Component', () => {
        let comp: ClientOrderSetitUiDetailComponent;
        let fixture: ComponentFixture<ClientOrderSetitUiDetailComponent>;
        let service: ClientOrderSetitUiService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [ClientOrderSetitUiDetailComponent],
                providers: [
                    ClientOrderSetitUiService
                ]
            })
            .overrideTemplate(ClientOrderSetitUiDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ClientOrderSetitUiDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClientOrderSetitUiService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new ClientOrderSetitUi(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.clientOrder).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
