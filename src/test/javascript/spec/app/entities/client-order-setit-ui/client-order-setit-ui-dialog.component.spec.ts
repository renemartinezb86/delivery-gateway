/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { GatewayTestModule } from '../../../test.module';
import { ClientOrderSetitUiDialogComponent } from '../../../../../../main/webapp/app/entities/client-order-setit-ui/client-order-setit-ui-dialog.component';
import { ClientOrderSetitUiService } from '../../../../../../main/webapp/app/entities/client-order-setit-ui/client-order-setit-ui.service';
import { ClientOrderSetitUi } from '../../../../../../main/webapp/app/entities/client-order-setit-ui/client-order-setit-ui.model';
import { ProviderSetitUiService } from '../../../../../../main/webapp/app/entities/provider-setit-ui';
import { AddressSetitUiService } from '../../../../../../main/webapp/app/entities/address-setit-ui';

describe('Component Tests', () => {

    describe('ClientOrderSetitUi Management Dialog Component', () => {
        let comp: ClientOrderSetitUiDialogComponent;
        let fixture: ComponentFixture<ClientOrderSetitUiDialogComponent>;
        let service: ClientOrderSetitUiService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [ClientOrderSetitUiDialogComponent],
                providers: [
                    ProviderSetitUiService,
                    AddressSetitUiService,
                    ClientOrderSetitUiService
                ]
            })
            .overrideTemplate(ClientOrderSetitUiDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ClientOrderSetitUiDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClientOrderSetitUiService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ClientOrderSetitUi(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.clientOrder = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'clientOrderListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ClientOrderSetitUi();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.clientOrder = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'clientOrderListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
