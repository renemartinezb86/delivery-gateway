/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { GatewayTestModule } from '../../../test.module';
import { LocationSetitUiDetailComponent } from '../../../../../../main/webapp/app/entities/location-setit-ui/location-setit-ui-detail.component';
import { LocationSetitUiService } from '../../../../../../main/webapp/app/entities/location-setit-ui/location-setit-ui.service';
import { LocationSetitUi } from '../../../../../../main/webapp/app/entities/location-setit-ui/location-setit-ui.model';

describe('Component Tests', () => {

    describe('LocationSetitUi Management Detail Component', () => {
        let comp: LocationSetitUiDetailComponent;
        let fixture: ComponentFixture<LocationSetitUiDetailComponent>;
        let service: LocationSetitUiService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [LocationSetitUiDetailComponent],
                providers: [
                    LocationSetitUiService
                ]
            })
            .overrideTemplate(LocationSetitUiDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LocationSetitUiDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LocationSetitUiService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new LocationSetitUi(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.location).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
