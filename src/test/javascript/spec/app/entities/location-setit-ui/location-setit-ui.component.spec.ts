/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GatewayTestModule } from '../../../test.module';
import { LocationSetitUiComponent } from '../../../../../../main/webapp/app/entities/location-setit-ui/location-setit-ui.component';
import { LocationSetitUiService } from '../../../../../../main/webapp/app/entities/location-setit-ui/location-setit-ui.service';
import { LocationSetitUi } from '../../../../../../main/webapp/app/entities/location-setit-ui/location-setit-ui.model';

describe('Component Tests', () => {

    describe('LocationSetitUi Management Component', () => {
        let comp: LocationSetitUiComponent;
        let fixture: ComponentFixture<LocationSetitUiComponent>;
        let service: LocationSetitUiService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [LocationSetitUiComponent],
                providers: [
                    LocationSetitUiService
                ]
            })
            .overrideTemplate(LocationSetitUiComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LocationSetitUiComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LocationSetitUiService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new LocationSetitUi(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.locations[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
