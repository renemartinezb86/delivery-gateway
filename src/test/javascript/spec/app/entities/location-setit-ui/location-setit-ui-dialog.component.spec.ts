/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { GatewayTestModule } from '../../../test.module';
import { LocationSetitUiDialogComponent } from '../../../../../../main/webapp/app/entities/location-setit-ui/location-setit-ui-dialog.component';
import { LocationSetitUiService } from '../../../../../../main/webapp/app/entities/location-setit-ui/location-setit-ui.service';
import { LocationSetitUi } from '../../../../../../main/webapp/app/entities/location-setit-ui/location-setit-ui.model';
import { RegionSetitUiService } from '../../../../../../main/webapp/app/entities/region-setit-ui';
import { ProviderSetitUiService } from '../../../../../../main/webapp/app/entities/provider-setit-ui';

describe('Component Tests', () => {

    describe('LocationSetitUi Management Dialog Component', () => {
        let comp: LocationSetitUiDialogComponent;
        let fixture: ComponentFixture<LocationSetitUiDialogComponent>;
        let service: LocationSetitUiService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [LocationSetitUiDialogComponent],
                providers: [
                    RegionSetitUiService,
                    ProviderSetitUiService,
                    LocationSetitUiService
                ]
            })
            .overrideTemplate(LocationSetitUiDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LocationSetitUiDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LocationSetitUiService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new LocationSetitUi(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.location = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'locationListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new LocationSetitUi();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.location = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'locationListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
