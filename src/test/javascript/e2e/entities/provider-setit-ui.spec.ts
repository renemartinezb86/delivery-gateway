import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Provider e2e test', () => {

    let navBarPage: NavBarPage;
    let providerDialogPage: ProviderDialogPage;
    let providerComponentsPage: ProviderComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Providers', () => {
        navBarPage.goToEntity('provider-setit-ui');
        providerComponentsPage = new ProviderComponentsPage();
        expect(providerComponentsPage.getTitle())
            .toMatch(/gatewayApp.provider.home.title/);

    });

    it('should load create Provider dialog', () => {
        providerComponentsPage.clickOnCreateButton();
        providerDialogPage = new ProviderDialogPage();
        expect(providerDialogPage.getModalTitle())
            .toMatch(/gatewayApp.provider.home.createOrEditLabel/);
        providerDialogPage.close();
    });

    it('should create and save Providers', () => {
        providerComponentsPage.clickOnCreateButton();
        providerDialogPage.setNameInput('name');
        expect(providerDialogPage.getNameInput()).toMatch('name');
        providerDialogPage.serviceTypeSelectLastOption();
        providerDialogPage.setEmailInput('email');
        expect(providerDialogPage.getEmailInput()).toMatch('email');
        providerDialogPage.setPhoneNumberInput('phoneNumber');
        expect(providerDialogPage.getPhoneNumberInput()).toMatch('phoneNumber');
        providerDialogPage.setUnitPriceInput('5');
        expect(providerDialogPage.getUnitPriceInput()).toMatch('5');
        providerDialogPage.setUnitFormatInput('unitFormat');
        expect(providerDialogPage.getUnitFormatInput()).toMatch('unitFormat');
        // providerDialogPage.locationSelectLastOption();
        providerDialogPage.save();
        expect(providerDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ProviderComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-provider-setit-ui div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ProviderDialogPage {
    modalTitle = element(by.css('h4#myProviderLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    serviceTypeSelect = element(by.css('select#field_serviceType'));
    emailInput = element(by.css('input#field_email'));
    phoneNumberInput = element(by.css('input#field_phoneNumber'));
    unitPriceInput = element(by.css('input#field_unitPrice'));
    unitFormatInput = element(by.css('input#field_unitFormat'));
    locationSelect = element(by.css('select#field_location'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    setServiceTypeSelect = function(serviceType) {
        this.serviceTypeSelect.sendKeys(serviceType);
    };

    getServiceTypeSelect = function() {
        return this.serviceTypeSelect.element(by.css('option:checked')).getText();
    };

    serviceTypeSelectLastOption = function() {
        this.serviceTypeSelect.all(by.tagName('option')).last().click();
    };
    setEmailInput = function(email) {
        this.emailInput.sendKeys(email);
    };

    getEmailInput = function() {
        return this.emailInput.getAttribute('value');
    };

    setPhoneNumberInput = function(phoneNumber) {
        this.phoneNumberInput.sendKeys(phoneNumber);
    };

    getPhoneNumberInput = function() {
        return this.phoneNumberInput.getAttribute('value');
    };

    setUnitPriceInput = function(unitPrice) {
        this.unitPriceInput.sendKeys(unitPrice);
    };

    getUnitPriceInput = function() {
        return this.unitPriceInput.getAttribute('value');
    };

    setUnitFormatInput = function(unitFormat) {
        this.unitFormatInput.sendKeys(unitFormat);
    };

    getUnitFormatInput = function() {
        return this.unitFormatInput.getAttribute('value');
    };

    locationSelectLastOption = function() {
        this.locationSelect.all(by.tagName('option')).last().click();
    };

    locationSelectOption = function(option) {
        this.locationSelect.sendKeys(option);
    };

    getLocationSelect = function() {
        return this.locationSelect;
    };

    getLocationSelectedOption = function() {
        return this.locationSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
