import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('ClientOrder e2e test', () => {

    let navBarPage: NavBarPage;
    let clientOrderDialogPage: ClientOrderDialogPage;
    let clientOrderComponentsPage: ClientOrderComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load ClientOrders', () => {
        navBarPage.goToEntity('client-order-setit-ui');
        clientOrderComponentsPage = new ClientOrderComponentsPage();
        expect(clientOrderComponentsPage.getTitle())
            .toMatch(/gatewayApp.clientOrder.home.title/);

    });

    it('should load create ClientOrder dialog', () => {
        clientOrderComponentsPage.clickOnCreateButton();
        clientOrderDialogPage = new ClientOrderDialogPage();
        expect(clientOrderDialogPage.getModalTitle())
            .toMatch(/gatewayApp.clientOrder.home.createOrEditLabel/);
        clientOrderDialogPage.close();
    });

    it('should create and save ClientOrders', () => {
        clientOrderComponentsPage.clickOnCreateButton();
        clientOrderDialogPage.setTitleInput('title');
        expect(clientOrderDialogPage.getTitleInput()).toMatch('title');
        clientOrderDialogPage.setCreateDateInput('2000-12-31');
        expect(clientOrderDialogPage.getCreateDateInput()).toMatch('2000-12-31');
        clientOrderDialogPage.setRequestDateInput('2000-12-31');
        expect(clientOrderDialogPage.getRequestDateInput()).toMatch('2000-12-31');
        clientOrderDialogPage.setFulfilmentDateInput('2000-12-31');
        expect(clientOrderDialogPage.getFulfilmentDateInput()).toMatch('2000-12-31');
        clientOrderDialogPage.serviceTypeSelectLastOption();
        clientOrderDialogPage.setQuantityInput('5');
        expect(clientOrderDialogPage.getQuantityInput()).toMatch('5');
        clientOrderDialogPage.setTotalPriceInput('5');
        expect(clientOrderDialogPage.getTotalPriceInput()).toMatch('5');
        clientOrderDialogPage.statusSelectLastOption();
        clientOrderDialogPage.providerSelectLastOption();
        clientOrderDialogPage.addressSelectLastOption();
        clientOrderDialogPage.save();
        expect(clientOrderDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ClientOrderComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-client-order-setit-ui div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ClientOrderDialogPage {
    modalTitle = element(by.css('h4#myClientOrderLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    titleInput = element(by.css('input#field_title'));
    createDateInput = element(by.css('input#field_createDate'));
    requestDateInput = element(by.css('input#field_requestDate'));
    fulfilmentDateInput = element(by.css('input#field_fulfilmentDate'));
    serviceTypeSelect = element(by.css('select#field_serviceType'));
    quantityInput = element(by.css('input#field_quantity'));
    totalPriceInput = element(by.css('input#field_totalPrice'));
    statusSelect = element(by.css('select#field_status'));
    providerSelect = element(by.css('select#field_provider'));
    addressSelect = element(by.css('select#field_address'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setTitleInput = function(title) {
        this.titleInput.sendKeys(title);
    };

    getTitleInput = function() {
        return this.titleInput.getAttribute('value');
    };

    setCreateDateInput = function(createDate) {
        this.createDateInput.sendKeys(createDate);
    };

    getCreateDateInput = function() {
        return this.createDateInput.getAttribute('value');
    };

    setRequestDateInput = function(requestDate) {
        this.requestDateInput.sendKeys(requestDate);
    };

    getRequestDateInput = function() {
        return this.requestDateInput.getAttribute('value');
    };

    setFulfilmentDateInput = function(fulfilmentDate) {
        this.fulfilmentDateInput.sendKeys(fulfilmentDate);
    };

    getFulfilmentDateInput = function() {
        return this.fulfilmentDateInput.getAttribute('value');
    };

    setServiceTypeSelect = function(serviceType) {
        this.serviceTypeSelect.sendKeys(serviceType);
    };

    getServiceTypeSelect = function() {
        return this.serviceTypeSelect.element(by.css('option:checked')).getText();
    };

    serviceTypeSelectLastOption = function() {
        this.serviceTypeSelect.all(by.tagName('option')).last().click();
    };
    setQuantityInput = function(quantity) {
        this.quantityInput.sendKeys(quantity);
    };

    getQuantityInput = function() {
        return this.quantityInput.getAttribute('value');
    };

    setTotalPriceInput = function(totalPrice) {
        this.totalPriceInput.sendKeys(totalPrice);
    };

    getTotalPriceInput = function() {
        return this.totalPriceInput.getAttribute('value');
    };

    setStatusSelect = function(status) {
        this.statusSelect.sendKeys(status);
    };

    getStatusSelect = function() {
        return this.statusSelect.element(by.css('option:checked')).getText();
    };

    statusSelectLastOption = function() {
        this.statusSelect.all(by.tagName('option')).last().click();
    };
    providerSelectLastOption = function() {
        this.providerSelect.all(by.tagName('option')).last().click();
    };

    providerSelectOption = function(option) {
        this.providerSelect.sendKeys(option);
    };

    getProviderSelect = function() {
        return this.providerSelect;
    };

    getProviderSelectedOption = function() {
        return this.providerSelect.element(by.css('option:checked')).getText();
    };

    addressSelectLastOption = function() {
        this.addressSelect.all(by.tagName('option')).last().click();
    };

    addressSelectOption = function(option) {
        this.addressSelect.sendKeys(option);
    };

    getAddressSelect = function() {
        return this.addressSelect;
    };

    getAddressSelectedOption = function() {
        return this.addressSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
