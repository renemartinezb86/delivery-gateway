import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Client e2e test', () => {

    let navBarPage: NavBarPage;
    let clientDialogPage: ClientDialogPage;
    let clientComponentsPage: ClientComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Clients', () => {
        navBarPage.goToEntity('client-setit-ui');
        clientComponentsPage = new ClientComponentsPage();
        expect(clientComponentsPage.getTitle())
            .toMatch(/gatewayApp.client.home.title/);

    });

    it('should load create Client dialog', () => {
        clientComponentsPage.clickOnCreateButton();
        clientDialogPage = new ClientDialogPage();
        expect(clientDialogPage.getModalTitle())
            .toMatch(/gatewayApp.client.home.createOrEditLabel/);
        clientDialogPage.close();
    });

    it('should create and save Clients', () => {
        clientComponentsPage.clickOnCreateButton();
        clientDialogPage.setNameInput('name');
        expect(clientDialogPage.getNameInput()).toMatch('name');
        clientDialogPage.setEmailInput('email');
        expect(clientDialogPage.getEmailInput()).toMatch('email');
        clientDialogPage.setPhoneNumberInput('phoneNumber');
        expect(clientDialogPage.getPhoneNumberInput()).toMatch('phoneNumber');
        clientDialogPage.paymentMethodSelectLastOption();
        clientDialogPage.submitMethodSelectLastOption();
        clientDialogPage.save();
        expect(clientDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ClientComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-client-setit-ui div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ClientDialogPage {
    modalTitle = element(by.css('h4#myClientLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    emailInput = element(by.css('input#field_email'));
    phoneNumberInput = element(by.css('input#field_phoneNumber'));
    paymentMethodSelect = element(by.css('select#field_paymentMethod'));
    submitMethodSelect = element(by.css('select#field_submitMethod'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    setEmailInput = function(email) {
        this.emailInput.sendKeys(email);
    };

    getEmailInput = function() {
        return this.emailInput.getAttribute('value');
    };

    setPhoneNumberInput = function(phoneNumber) {
        this.phoneNumberInput.sendKeys(phoneNumber);
    };

    getPhoneNumberInput = function() {
        return this.phoneNumberInput.getAttribute('value');
    };

    setPaymentMethodSelect = function(paymentMethod) {
        this.paymentMethodSelect.sendKeys(paymentMethod);
    };

    getPaymentMethodSelect = function() {
        return this.paymentMethodSelect.element(by.css('option:checked')).getText();
    };

    paymentMethodSelectLastOption = function() {
        this.paymentMethodSelect.all(by.tagName('option')).last().click();
    };
    setSubmitMethodSelect = function(submitMethod) {
        this.submitMethodSelect.sendKeys(submitMethod);
    };

    getSubmitMethodSelect = function() {
        return this.submitMethodSelect.element(by.css('option:checked')).getText();
    };

    submitMethodSelectLastOption = function() {
        this.submitMethodSelect.all(by.tagName('option')).last().click();
    };
    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
