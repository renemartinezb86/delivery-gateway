import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Rating e2e test', () => {

    let navBarPage: NavBarPage;
    let ratingDialogPage: RatingDialogPage;
    let ratingComponentsPage: RatingComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Ratings', () => {
        navBarPage.goToEntity('rating-setit-ui');
        ratingComponentsPage = new RatingComponentsPage();
        expect(ratingComponentsPage.getTitle())
            .toMatch(/gatewayApp.rating.home.title/);

    });

    it('should load create Rating dialog', () => {
        ratingComponentsPage.clickOnCreateButton();
        ratingDialogPage = new RatingDialogPage();
        expect(ratingDialogPage.getModalTitle())
            .toMatch(/gatewayApp.rating.home.createOrEditLabel/);
        ratingDialogPage.close();
    });

    it('should create and save Ratings', () => {
        ratingComponentsPage.clickOnCreateButton();
        ratingDialogPage.setValueInput('5');
        expect(ratingDialogPage.getValueInput()).toMatch('5');
        ratingDialogPage.setCommentsInput('comments');
        expect(ratingDialogPage.getCommentsInput()).toMatch('comments');
        ratingDialogPage.clientOrderSelectLastOption();
        ratingDialogPage.save();
        expect(ratingDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class RatingComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-rating-setit-ui div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class RatingDialogPage {
    modalTitle = element(by.css('h4#myRatingLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    valueInput = element(by.css('input#field_value'));
    commentsInput = element(by.css('input#field_comments'));
    clientOrderSelect = element(by.css('select#field_clientOrder'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setValueInput = function(value) {
        this.valueInput.sendKeys(value);
    };

    getValueInput = function() {
        return this.valueInput.getAttribute('value');
    };

    setCommentsInput = function(comments) {
        this.commentsInput.sendKeys(comments);
    };

    getCommentsInput = function() {
        return this.commentsInput.getAttribute('value');
    };

    clientOrderSelectLastOption = function() {
        this.clientOrderSelect.all(by.tagName('option')).last().click();
    };

    clientOrderSelectOption = function(option) {
        this.clientOrderSelect.sendKeys(option);
    };

    getClientOrderSelect = function() {
        return this.clientOrderSelect;
    };

    getClientOrderSelectedOption = function() {
        return this.clientOrderSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
